---
layout: markdown_page
title: "Contributing to Documentation"
description: "This section pertains to documentation changes that are independent of other code/feature changes."
canonical_path: "/community/contribute/documentation/"
---

## Documentation

GitLab welcomes contributions to the product documentation.


### If you need an issue to work on

View the [documentation issues curated specifically for new contributors](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=documentation&label_name%5B%5D=docs-only&label_name%5B%5D=Seeking%20community%20contributions&first_page_size=20).

From the list, select an issue you'd like to work on.

- If the issue is already assigned to someone, pick a different one.
- If the issue is unassigned, add a comment and ask to work on the issue.

  For a Hackathon, use `@gitlab-org/docs-hackathon`. Otherwise, use `@gl-docsteam`. For example:

  ```plaintext
  @gitlab-org/docs-hackathon I would like to work on this issue
  ``` 

- Do not ask for more than three issues at a time.

After the issue is assigned to you, open a merge request to address it.
If you've never contributed to the GitLab documentation before, [view instructions for how to do it](https://docs.gitlab.com/ee/development/documentation/workflow.html).

### If you want to explore on your own

If you don't find an open issue to work on, you can still update the documentation.

Go to <https://docs.gitlab.com> and at the bottom of any page, select **View page source** or **Edit in Web IDE**.

You can also:

- Choose a page [in the `/doc` directory](https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc)
  and edit it from there.
- Try installing and running the [Vale linting tool](https://docs.gitlab.com/ee/development/documentation/testing.html#vale)
  and fixing the resulting issues.

### If you're updating docs as part of code development

When you're developing code, the workflow for updating docs is slightly different.

For details, see the [Contributing to Development page](/community/contribute/development/index.html).
