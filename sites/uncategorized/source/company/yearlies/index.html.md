---
layout: handbook-page-toc
title: "Yearlies"
description: ""
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Yearlies

Yearlies are the annual goals for the company. Yearlies should have measurable deliverables.

## Alignment 

### Yearlies and Three Year Strategy 

1. Yearlies are informed by the [three-year strategy](https://about.gitlab.com/company/strategy/). 
1. Like [Objectives and Key Results (OKRs)](https://about.gitlab.com/company/okrs/), each yearly is aligned to one of the three pillars of the [three year strategy](/company/strategy/#three-year-strategy). 
1. There should be at least one yearly for each strategic pillar from [three year strategy](/company/strategy/#three-year-strategy).

### Difference between Annual Plan and Yearlies

1. Yearlies come before the [Annual Plan](/handbook/finance/financial-planning-and-analysis/#plan). 
1. Yearlies contain our priorities for the fiscal year while the Annual Plan contains our budgets and our financials.  
1. We first determine our priorities for the upcoming year in the form of Yearlies, then we use these priorities to inform the budget in the Annual Plan process. 

The Annual Plan process [finishes two quarters after](https://about.gitlab.com/company/offsite/#offsite-topic-calendar) Yearlies are finalized. As a result, Yearlies may become outdated since being established. In line with our value of [Iteration](https://about.gitlab.com/handbook/values/#iteration), Yearlies are [reviewed each E-Group offsite](https://about.gitlab.com/company/offsite/#recurring-discussion-topics) and updated as needed. Annual Plan and Yearlies should be synced when Annual Plan is finalized.

### Difference between OKRs and Yearlies

1. [OKRs](https://about.gitlab.com/company/okrs/) have a duration of one quarter while Yearlies are annual goals with a duration of a year.
1. [OKRs](https://about.gitlab.com/company/okrs/) are composed of Objectives and Key Results while Yearlies have only one component, the annual goal.

## Cadence

1. The [three year strategy](/company/strategy/#three-year-strategy) is on a 3 year cadence and is inspiration for the Yearlies, which are on a 1 year [cadence](/company/cadence/#year). The three year strategy is reviewed as part of [E-Group offsite calendar](/offsite/#offsite-topic-calendar). 
1. Yearlies are established during E-Group offsite as a once-a-year topic in the [offsite topic calendar](https://about.gitlab.com/company/offsite/#offsite-topic-calendar). 
1. Yearlies are the company's goals for the next 12 months unless the Yearly is achieved sooner than 12 months or E-Group decides that a Yearly should be updated. 
1. In line with our value of [Iteration](https://about.gitlab.com/handbook/values/#iteration), Yearlies are reviewed during each E-Group offsite as a [recurring discussion topic](/company/offsite/#recurring-discussion-topics) and updated or replaced if the Yearly needs to be changed sooner than 12 months after being established. 

## FY24 Yearlies

### Strategic Pillar 1: [Customer Results](/company/strategy/#1-customer-results) 

Improve customers results through:
1. Ultimate Enablement
1. Increasing Sales Efficiency
1. Increasing User Awareness

### Strategic Pillar 2: [Mature the Platform](/company/strategy/#2-mature-the-platform)

Mature the Platform by: 
1. Closing BIC gaps
1. GitLab Dedicated GA
1. Scaling GitLab.com

### Strategic Pillar 3: [Grow Careers](/company/strategy/#3-grow-careers) 

Career Building Platform through:
1. Faster Pace 
1. Increase Team Diversity
1. Enabling People Managers through [Elevate](/handbook/people-group/learning-and-development/elevate/)
